package user

type User struct{
	Id int64
	Name string
	Email string
}


func Make(id int64, name string, email string) *User {
	return &User{
		Id: id,
		Name: name, 
		Email: email,
	}
}