package api

import "fmt"
import "net/http"
import "network/module/user"

func User(w http.ResponseWriter, r *http.Request) {

	user := user.Make(1,"Ali","web-ali@yandex.ru")

	fmt.Fprintf(w, "Hello " + user.Name)
}