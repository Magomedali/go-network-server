package App

import (
	"log"
	"net/http"
	"network/http/controller"
	"network/http/controller/api"
)

type Config struct{
	Port int16
}

func Init() {

	configureRoutes()
	runServer()
}

func configureRoutes() {
	
	http.HandleFunc("/", controller.Home)
	http.HandleFunc("/api", api.Version)
	http.HandleFunc("/api/user", api.User)

}

func runServer() {
	err := http.ListenAndServe(":8900", nil)

	if err != nil {
		log.Fatal("ListenAndServe", err)
	}
}