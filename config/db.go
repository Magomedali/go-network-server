package config

type DBConfig struct {
	Host string
	Port int
	Database string
	Username string
	Password string
}