package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	"strconv"
)

type Config struct{
	DB DBConfig
	Listen string
}

func (c *Config) Configure(){
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	c.DB.Host = os.Getenv("DB_HOST")
	c.DB.Port, err = strconv.Atoi(os.Getenv("DB_PORT"))
	if err != nil {
		log.Fatal("Error convert db port value")
	}
	c.DB.Database = os.Getenv("DB_NAME")
	c.DB.Username = os.Getenv("DB_USER")
	c.DB.Password = os.Getenv("DB_PASSWORD")

	c.Listen = os.Getenv("LISTEN")
}