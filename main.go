package main

import "network/http/App"
import "network/config"

var AppConfig config.Config

func main() {

	AppConfig.Configure()


	App.Init()

}